import { MagicCardSitePage } from './app.po';

describe('magic-card-site App', () => {
  let page: MagicCardSitePage;

  beforeEach(() => {
    page = new MagicCardSitePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
